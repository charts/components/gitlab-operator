# GitLab Chart Kubernetes Operator

The repo location for the operator was moved in September 2019 to be under the gitab-org group at: https://gitlab.com/gitlab-org/charts/components/gitlab-operator/

Please use the new location.
